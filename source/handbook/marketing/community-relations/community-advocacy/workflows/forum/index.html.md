---
layout: markdown_page
title: "Forum response workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

New posts to the GitLab forum are brought in to Zendesk and #gitlab-forum on Slack

## Best practices

- Always be courteous, especially if they are a new poster.
- Add a welcome message when replying to new posters. It could be as simple as "Hi, and welcome to the forum! :smile:" at the start of your post.
- Use the like button as much as you can, to thank users for their input and to inspire other users to do the same.

## Workflow

Follow the relevant workflow depending on the question:

```mermaid
graph TD
A((Non-support)) --> B
B(Account) --> |Sales/Renewal| E[ping #sales]
B --> |Other|F(Have they opened a support ticket?)
F --> |Yes, using GitLab.com| G[ping #support_gitlab-com]
F --> |Yes, self-managed| H[ping #support_self-managed]
F --> |No| I[Direct them to support.gitlab.com]
A --> J(GitLab vs Competitor)
J --> |Question|K[Find answer via devops tools page/blog and link]
J --> |Discussion|L[Link on #competition]
A --> M(Feature Proposal)
M --> |Already exists|N[Link to the open issue]
M --> |Doesn't exist|O[Ask them to open an issue]
```

```mermaid
graph TD
A((Support)) --> B(What level of GitLab are they on?)
B --> |Not free|C(Have they opened a support ticket?)
C -->|No| E[Link them to the support portal]
C -->|Yes| F(Have they provided the ticket number?)
F --> |No|R(Ask them for the ticket number)
R --> F
F --> |Yes|G(Self-hosted or .com?)
G --> |Self-hosted|H[ping #support_self-managed]
G --> |.com|I[ping #support_gitlab-com]
G --> |not provided|J(Ask what they're using)
J --> G
B-->|Free| D(Is there an issue open already?)
D --> |Yes|K[Link the issue]
D --> |ask the following questions|L(Self-hosted or .com?)
L --> M(What version are they on?)
M --> N(Has this happened on a previous version?)
N --> O(How long have they been experiencing it?)
O --> P(What integrations do they have, if relevant?)
P --> |Questions answered|Q[ping relevant product channel]
```


## Automation

New mentions are brought into Slack and Zendesk via Zapier.
