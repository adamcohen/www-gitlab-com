---
layout: markdown_page
title: "IAM.6.01 - Key Repository Access Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.6.01 - Key Repository Access

## Control Statement

Access to the cryptographic keystores is limited to authorized personnel.

## Context

One of the fundamental and most important security considerations of encryption is protecting cryptographic keys, particularly private keys. This controls aims to protect the confidentiality and integrity of data for customers, GitLab team-members, and partners by limiting the number of people with access to view, modify, and create new cryptographic keys. A malicious actor with access to GitLab's cryptographic keys could decrypt all sensitive data stored and transmitted by GitLab, rending the encryption useless.

## Scope

This control applies to any and all cryptographic keystores.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.6.01_key_repository_access.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.6.01_key_repository_access.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.6.01_key_repository_access.md).

## Framework Mapping

* ISO
  * A.10.1.2
  * A.18.1.5
* SOC2 CC
  * CC6.1
  * CC6.3
* PCI
  * 3.5
  * 3.5.2
  * 3.6
  * 3.6.2
  * 3.6.3
  * 3.6.7
